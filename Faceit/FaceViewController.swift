//
//  ViewController.swift
//  Faceit
//
//  Created by Yijia Huang on 8/20/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

class FaceViewController: VCLLoggingViewController {



    @IBOutlet weak var faceView: FaceView! {
        didSet {
            let handler = #selector(FaceView.changeScale(byReactingTo:))
            let pinchRecognizer = UIPinchGestureRecognizer(target: faceView, action: handler)
            faceView.addGestureRecognizer(pinchRecognizer)
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleEyes(byReactingTo:)))
            tapRecognizer.numberOfTapsRequired = 1
            faceView.addGestureRecognizer(tapRecognizer)
            let swipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(increaseHappiness))
            faceView.addGestureRecognizer(swipeUpRecognizer)
            swipeUpRecognizer.direction = .up
            let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(decreaseHappiness))
            swipeDownRecognizer.direction = .down
            faceView.addGestureRecognizer(swipeDownRecognizer)
            updateUI()
        }
    }
    
    func increaseHappiness() {
        expression = expression.happier
    }
    
    func decreaseHappiness() {
        expression = expression.sadder
    }
    
    func toggleEyes(byReactingTo tapRecognizer: UITapGestureRecognizer) {
        if tapRecognizer.state == .ended {
            let eyes: FacicalExpression.Eyes = (expression.eyes == .closed) ? .open : .closed
            expression = FacicalExpression(eyes: eyes, mouth: expression.mouth)
        }
    }
    
    var expression = FacicalExpression(eyes: .closed, mouth: .frown) {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI() {
        switch expression.eyes {
        case .open:
            faceView?.eyesOpen = true
        case .closed:
            faceView?.eyesOpen = false
        case .squinting:
            faceView?.eyesOpen = false
        }
        faceView?.mouthCurvature = mouthCurvatures[expression.mouth] ?? 0.0
    }
    
    private let mouthCurvatures = [FacicalExpression.Mouth.grin: 0.5, .frown:-1.0,.smile:1.0,.neutral:0.0,.smirk:-0.5]
    
    @IBAction func punch(_ sender: UIButton) {
        performSegue(withIdentifier: "goto", sender: sender)
    }

}

