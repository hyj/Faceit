//
//  SecondVC.swift
//  Faceit
//
//  Created by Yijia Huang on 8/25/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func relieve(_ sender: UIButton) {
        performSegue(withIdentifier: "goback", sender: sender)
    }

}
