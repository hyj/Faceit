//
//  EmotionsViewController.swift
//  Faceit
//
//  Created by Yijia Huang on 9/4/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

class EmotionsViewController: VCLLoggingViewController {
    
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationViewController = segue.destination
        if let navigationController = destinationViewController as? UINavigationController {
            destinationViewController = navigationController.visibleViewController ?? destinationViewController
        }
        if let faceViewController = destinationViewController as? FaceViewController,
           let identifier = segue.identifier,
           let expression = emotionalFaces[identifier] {
                faceViewController.expression = expression
                faceViewController.navigationItem.title = (sender as? UIButton)?.currentTitle
        }
    }
 
    private let emotionalFaces: Dictionary<String, FacicalExpression> = [
        "sad" : FacicalExpression(eyes: .closed, mouth: .frown),
        "happy" : FacicalExpression(eyes: .open, mouth: .smile),
        "worried" : FacicalExpression(eyes: .open, mouth: .smirk)
    ]
}
