//
//  File.swift
//  Faceit
//
//  Created by Yijia Huang on 8/28/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import Foundation

struct FacicalExpression {
    enum Eyes: Int {
        case open
        case closed
        case squinting
    }
    
    enum Mouth: Int {
        case frown
        case smirk
        case neutral
        case grin
        case smile
        
        var sadder: Mouth {
            return Mouth(rawValue: rawValue - 1) ?? .frown
        }
        var happier: Mouth {
            return Mouth(rawValue: rawValue + 1) ?? .smile
        }
    }
    
    var sadder: FacicalExpression {
        return FacicalExpression(eyes: self.eyes, mouth: self.mouth.sadder)
    }
    var happier: FacicalExpression {
        return FacicalExpression(eyes: self.eyes, mouth: self.mouth.happier)
    }
    
    let eyes: Eyes
    let mouth: Mouth
}
